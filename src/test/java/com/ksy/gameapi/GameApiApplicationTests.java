package com.ksy.gameapi;

import com.ksy.gameapi.entity.Puppy;
import com.ksy.gameapi.model.ChoosePuppyItem;
import com.ksy.gameapi.repository.PuppyRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.LinkedList;
import java.util.List;

@SpringBootTest
class GameApiApplicationTests {
    @Autowired
    private PuppyRepository puppyRepository;

    @Test
    void contextLoads() {
    }

    @Test
    void testPercentTwo() { // 지금 멀 할거냐면... 뽑기를 할거야. 그러려면 확률표도 만들어야 하고, 뽑기 기능도 만들어야겠지... 랜덤 이용해서
        List<Puppy> puppies = puppyRepository.findAll(); // 일단 원본 파일을 불러와 확률은 퍼피에 있다

        List<ChoosePuppyItem> percent = new LinkedList<>(); // 퍼센트 최소, 최대값 담을 리스트 생성
        double oldPercent = 0D; // 일단 기본 퍼센트값은 0이야 여기다 차곡차곡 추가한다
        for (Puppy puppy : puppies) { // 퍼피를 하나하나 나눠서 여기 있는 아이디와 부스러기 값을 가져와 담는 거지
            ChoosePuppyItem addItem = new ChoosePuppyItem();
            addItem.setId(puppy.getId());
            addItem.setPercentMin(oldPercent);
            addItem.setPercentMax(oldPercent + puppy.getCrumbsPercent());

            percent.add(addItem);

            oldPercent += puppy.getCrumbsPercent();
        }


    }

    @Test
    void testPercent() {
        List<Puppy> puppies = puppyRepository.findAll();

        List<ChoosePuppyItem> percentBar = new LinkedList<>();

        double oldPercent = 0D; // 부스러기 퍼센트 바 만드는 중!!
        for (Puppy puppy : puppies) {
            ChoosePuppyItem addItem = new ChoosePuppyItem();
            addItem.setId(puppy.getId());
            addItem.setPercentMin(oldPercent);
            addItem.setPercentMax(oldPercent + puppy.getCrumbsPercent());

            percentBar.add(addItem);

            oldPercent += puppy.getCrumbsPercent();
        }

        double percentResult = Math.random() * 100;

        long resultId = 0;
        for (ChoosePuppyItem item : percentBar) {
            if (percentResult >= item.getPercentMin() && percentResult <= item.getPercentMax()) {
                resultId = item.getId();
                break;
            }
        }

        System.out.println(resultId);
    }
}