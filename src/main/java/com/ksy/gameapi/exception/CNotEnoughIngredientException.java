package com.ksy.gameapi.exception;

public class CNotEnoughIngredientException extends RuntimeException {
    public CNotEnoughIngredientException(String msg, Throwable t) {
        super(msg, t);
    }

    public CNotEnoughIngredientException(String msg) {
        super(msg);
    }

    public CNotEnoughIngredientException() {
        super();
    }
}
