package com.ksy.gameapi.controller;

import com.ksy.gameapi.model.IngredientResponse;
import com.ksy.gameapi.model.SingleResult;
import com.ksy.gameapi.service.IngredientService;
import com.ksy.gameapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "재료 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/ingredient")
public class IngredientController {
    private final IngredientService ingredientService;

    @ApiOperation(value = "재료 증가(재료 모으기)")
    @PutMapping("/plus")
    public SingleResult<IngredientResponse> plusIngredient() {
    return ResponseService.getSingleResult(ingredientService.putIngredientPlus());
    }
}
