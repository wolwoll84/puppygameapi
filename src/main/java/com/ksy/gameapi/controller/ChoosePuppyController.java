package com.ksy.gameapi.controller;

import com.ksy.gameapi.model.ChoosePuppyResponse;
import com.ksy.gameapi.model.SingleResult;
import com.ksy.gameapi.service.ChoosePuppyService;
import com.ksy.gameapi.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "강아지 뽑기")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/choose-puppy")
public class ChoosePuppyController {
    private final ChoosePuppyService choosePuppyService;

    @ApiOperation(value = "부스러기 뽑기")
    @PostMapping("/crumbs")
    public SingleResult<ChoosePuppyResponse> chooseCrumbs() {
        return ResponseService.getSingleResult(choosePuppyService.getResult(false));
    }

    @ApiOperation(value = "푸짐한상 뽑기")
    @PostMapping("/big")
    public SingleResult<ChoosePuppyResponse> chooseBig() {
        return ResponseService.getSingleResult(choosePuppyService.getResult(true));
    }
}
