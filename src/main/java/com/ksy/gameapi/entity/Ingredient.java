package com.ksy.gameapi.entity;

import com.ksy.gameapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Ingredient {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "재료 수량")
    @Column(nullable = false)
    private Long ingredientCount;

    public void resetIngredient() {
        this.ingredientCount = 0L;
    }

    public void plusIngredient() {
        this.ingredientCount += 10000;
    }

    public void minusIngredient(Long chooseIngredient) {
        this.ingredientCount -= chooseIngredient;
    }

    private Ingredient(IngredientBuilder builder) {
        this.ingredientCount = builder.ingredientCount;
    }

    public static class IngredientBuilder implements CommonModelBuilder<Ingredient> {
        private final Long ingredientCount;

        public IngredientBuilder() {
            this.ingredientCount = 0L;
        }

        @Override
        public Ingredient build() {
            return new Ingredient(this);
        }
    }
}
