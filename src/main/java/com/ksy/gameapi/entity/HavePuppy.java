package com.ksy.gameapi.entity;

import com.ksy.gameapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HavePuppy {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "강아지 id")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "puppyId", nullable = false)
    private Puppy puppy;

    @ApiModelProperty(notes = "보유한 강아지 수량")
    @Column(nullable = false)
    private Integer puppyCount;

    public void resetCount() {
        this.puppyCount = 0;
    }

    public void putCountPlus() {
        this.puppyCount += 1;
    }

    private HavePuppy(HavePuppyBuilder builder) {
        this.puppy = builder.puppy;
        this.puppyCount = builder.puppyCount;
    }

    public static class HavePuppyBuilder implements CommonModelBuilder<HavePuppy> {
        private final Puppy puppy;
        private final Integer puppyCount;

        public HavePuppyBuilder(Puppy puppy) {
            this.puppy = puppy;
            this.puppyCount = 0;
        }

        @Override
        public HavePuppy build() {
            return new HavePuppy(this);
        }
    }
}
