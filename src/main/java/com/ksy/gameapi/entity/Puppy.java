package com.ksy.gameapi.entity;

import com.ksy.gameapi.enums.Rating;
import com.ksy.gameapi.interfaces.CommonModelBuilder;
import com.ksy.gameapi.model.PuppyRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Puppy {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "등급")
    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 10)
    private Rating rating;

    @ApiModelProperty(notes = "강아지 이름")
    @Column(nullable = false, length = 25)
    private String puppyName;

    @ApiModelProperty(notes = "이미지 이름")
    @Column(nullable = false, length = 50)
    private String imageName;

    @ApiModelProperty(notes = "부스러기 확률")
    @Column(nullable = false)
    private Double crumbsPercent;

    @ApiModelProperty(notes = "푸짐한상 확률")
    @Column(nullable = false)
    private Double bigPercent;

    private Puppy(PuppyBuilder builder) {
        this.rating = builder.rating;
        this.puppyName = builder.puppyName;
        this.imageName = builder.imageName;
        this.crumbsPercent = builder.crumbsPercent;
        this.bigPercent = builder.bigPercent;
    }

    public static class PuppyBuilder implements CommonModelBuilder<Puppy> {
        private final Rating rating;
        private final String puppyName;
        private final String imageName;
        private final Double crumbsPercent;
        private final Double bigPercent;

        public PuppyBuilder(PuppyRequest request) {
            this.rating = request.getRating();
            this.puppyName = request.getPuppyName();
            this.imageName = request.getImageName();
            this.crumbsPercent = request.getCrumbsPercent();
            this.bigPercent = request.getBigPercent();
        }

        @Override
        public Puppy build() {
            return new Puppy(this);
        }
    }
}
