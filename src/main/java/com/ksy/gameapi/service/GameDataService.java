package com.ksy.gameapi.service;

import com.ksy.gameapi.entity.HavePuppy;
import com.ksy.gameapi.entity.Ingredient;
import com.ksy.gameapi.exception.CMissingDataException;
import com.ksy.gameapi.model.FirstConnectDataResponse;
import com.ksy.gameapi.model.HavePuppyItem;
import com.ksy.gameapi.model.IngredientResponse;
import com.ksy.gameapi.repository.HavePuppyRepository;
import com.ksy.gameapi.repository.IngredientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GameDataService {
    private final IngredientRepository ingredientRepository;
    private final HavePuppyRepository havePuppyRepository;

    public FirstConnectDataResponse getFirstData() {
        FirstConnectDataResponse result = new FirstConnectDataResponse(); // setter 쓰니까 빈 그릇 생성

        Ingredient ingredient = ingredientRepository.findById(1L).orElseThrow(CMissingDataException::new); // 돈 원본 데이터 가져오고
        result.setIngredientResponse(new IngredientResponse.IngredientResponseBuilder(ingredient).build()); // 결과에 입력

        result.setHavePuppyItems(this.getMyPuppies());

        return result;
    }

    public List<HavePuppyItem> getMyPuppies() {
        List<HavePuppy> originList = havePuppyRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1L);

        List<HavePuppyItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new HavePuppyItem.HavePuppyItemBuilder(item).build()));

        return result;
    }

    public void resetGame() {
        Ingredient ingredient = ingredientRepository.findById(1L).orElseThrow(CMissingDataException::new);
        ingredient.resetIngredient();
        ingredientRepository.save(ingredient);

        List<HavePuppy> havePuppy = havePuppyRepository.findAll();
        for (HavePuppy puppy : havePuppy) {
            puppy.resetCount();
            havePuppyRepository.save(puppy);
        }
    }
}
