package com.ksy.gameapi.service;

import com.ksy.gameapi.entity.HavePuppy;
import com.ksy.gameapi.entity.Ingredient;
import com.ksy.gameapi.entity.Puppy;
import com.ksy.gameapi.exception.CMissingDataException;
import com.ksy.gameapi.exception.CNotEnoughIngredientException;
import com.ksy.gameapi.model.*;
import com.ksy.gameapi.repository.HavePuppyRepository;
import com.ksy.gameapi.repository.IngredientRepository;
import com.ksy.gameapi.repository.PuppyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ChoosePuppyService {
    private final IngredientRepository ingredientRepository;
    private final PuppyRepository puppyRepository;
    private final HavePuppyRepository havePuppyRepository;

    long chooseIngredient = 0L;

    private void init(boolean isBig) {
        this.chooseIngredient = isBig? 25000L : 5000L;
    }

    // 팀장
    public ChoosePuppyResponse getResult(boolean isBig) {
        this.init(isBig); // 뽑기 금액 넣었어

        boolean isEnoughIngredient = this.isStartChooseByIngredient(); // 돈 체크하는 애가 주는 부울린값 받을 거 만들어놔야지

        if (!isEnoughIngredient) throw new CNotEnoughIngredientException(); // 돈 부족하면 에러

        Ingredient ingredient = ingredientRepository.findById(1L).orElseThrow(CMissingDataException::new);
        ingredient.minusIngredient(chooseIngredient);
        ingredientRepository.save(ingredient);

        long puppyResultId = this.getChoosePuppy(isBig);

        boolean isNewPuppy = false;
        Optional<HavePuppy> havePuppy = havePuppyRepository.findByPuppy_Id(puppyResultId);
        if (havePuppy.isEmpty()) throw new CMissingDataException();
        HavePuppy havePuppyData = havePuppy.get(); // 명확성을 위해 havePuppy 모양 상자를 만들어서 원본 데이터 옮겨두기. ***
        // 위에서 불러온 havePuppy 에 바로 넣지 않고 옮겨 담아야 하는 이유를 정확히 이해하지 못하겠음... 강의 다시 보기
        // 헷갈리니까~!!!!

        if (havePuppyData.getPuppyCount() == 0) isNewPuppy = true;

        havePuppyData.putCountPlus();
        havePuppyRepository.save(havePuppyData);

        ChoosePuppyResponse result = new ChoosePuppyResponse();
        result.setIngredientResponse(new IngredientResponse.IngredientResponseBuilder(ingredient).build());
        result.setCurrentItem(new ChoosePuppyCurrentItem.ChoosePuppyCurrentItemBuilder(havePuppyData.getPuppy(), isNewPuppy).build());
        result.setHavePuppyItems(this.getMyPuppies());

        return result;
    }

    public List<HavePuppyItem> getMyPuppies() {
        List<HavePuppy> originList = havePuppyRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1L);

        List<HavePuppyItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new HavePuppyItem.HavePuppyItemBuilder(item).build()));

        return result;
    }

    private boolean isStartChooseByIngredient() { // 돈이 되는지 체크하는 애
        Ingredient ingredient = ingredientRepository.findById(1L).orElseThrow(CMissingDataException::new);

        boolean result = false;

        if (ingredient.getIngredientCount() >= chooseIngredient) result = true;

        return result;
    }

    private long getChoosePuppy(boolean isBig) { // 유물 뽑아주는 애는 만들었어
        List<Puppy> puppies = puppyRepository.findAll();

        List<ChoosePuppyItem> percentBar = new LinkedList<>();

        double oldPercent = 0D;
        for (Puppy puppy : puppies) {
            ChoosePuppyItem addItem = new ChoosePuppyItem();
            addItem.setId(puppy.getId());
            addItem.setPercentMin(oldPercent);
            if (isBig) {
                addItem.setPercentMax(oldPercent + puppy.getBigPercent());
            } else {
                addItem.setPercentMax(oldPercent + puppy.getCrumbsPercent());
            }

            percentBar.add(addItem);

            if (isBig) {
                oldPercent += puppy.getBigPercent();
            } else {
                oldPercent += puppy.getCrumbsPercent();
            }

        }

        double percentResult = Math.random() * 100;

        long resultId = 0;
        for (ChoosePuppyItem item : percentBar) {
            if (percentResult >= item.getPercentMin() && percentResult <= item.getPercentMax()) {
                resultId = item.getId();
                break;
            }
        }

        return resultId;
    }
}
