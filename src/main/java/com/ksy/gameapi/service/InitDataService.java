package com.ksy.gameapi.service;

import com.ksy.gameapi.entity.HavePuppy;
import com.ksy.gameapi.entity.Ingredient;
import com.ksy.gameapi.entity.Puppy;
import com.ksy.gameapi.enums.Rating;
import com.ksy.gameapi.model.PuppyRequest;
import com.ksy.gameapi.repository.HavePuppyRepository;
import com.ksy.gameapi.repository.IngredientRepository;
import com.ksy.gameapi.repository.PuppyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class InitDataService {
    private final IngredientRepository ingredientRepository;
    private final PuppyRepository puppyRepository;
    private final HavePuppyRepository havePuppyRepository;

    public void setFirstIngredient() {
        Optional<Ingredient> originData = ingredientRepository.findById(1L);

        if (originData.isEmpty()) {
            Ingredient addData = new Ingredient.IngredientBuilder().build();
            ingredientRepository.save(addData);
        }
    }

    public void setFirstPuppy() {
        List<Puppy> originList = puppyRepository.findAll();

        if (originList.size() == 0) {
            List<PuppyRequest> result = new LinkedList<>();
            PuppyRequest request1 = new PuppyRequest.PuppyRequestBuilder(Rating.NORMAL, "목욕하는 강아지", "dog1.jpg", 11D, 0D).build();
            result.add(request1);
            PuppyRequest request2 = new PuppyRequest.PuppyRequestBuilder(Rating.NORMAL, "잔소리하는 강아지", "dog2.jpg", 11D, 0D).build();
            result.add(request2);
            PuppyRequest request3 = new PuppyRequest.PuppyRequestBuilder(Rating.NORMAL, "찌그러진 강아지", "dog3.jpg", 11D, 0D).build();
            result.add(request3);
            PuppyRequest request4 = new PuppyRequest.PuppyRequestBuilder(Rating.NORMAL, "휘핑크림을 얹은 강아지", "dog4.jpg", 11D, 0D).build();
            result.add(request4);
            PuppyRequest request5 = new PuppyRequest.PuppyRequestBuilder(Rating.NORMAL, "곰이 된 강아지", "dog5.jpg", 11D, 0D).build();
            result.add(request5);
            PuppyRequest request6 = new PuppyRequest.PuppyRequestBuilder(Rating.NORMAL, "댕삐진 강아지", "dog6.jpg", 11D, 0D).build();
            result.add(request6);
            PuppyRequest request7 = new PuppyRequest.PuppyRequestBuilder(Rating.NORMAL, "선글라스 강아지", "dog7.jpg", 11D, 0D).build();
            result.add(request7);
            PuppyRequest request8 = new PuppyRequest.PuppyRequestBuilder(Rating.NORMAL, "대학원생 강아지", "dog8.jpg", 11D, 0D).build();
            result.add(request8);
            PuppyRequest request9 = new PuppyRequest.PuppyRequestBuilder(Rating.RARE, "입맛을 다시는 강아지", "dog9.jpg", 1.5D, 10D).build();
            result.add(request9);
            PuppyRequest request10 = new PuppyRequest.PuppyRequestBuilder(Rating.RARE, "고구마 농부 강아지", "dog10.jpg", 1.5D, 10D).build();
            result.add(request10);
            PuppyRequest request11 = new PuppyRequest.PuppyRequestBuilder(Rating.RARE, "콧물 흘리는 시루떡", "dog11.jpg", 1.5D, 10D).build();
            result.add(request11);
            PuppyRequest request12 = new PuppyRequest.PuppyRequestBuilder(Rating.RARE, "떼어먹힌 강아지", "dog12.jpg", 1.5D, 10D).build();
            result.add(request12);
            PuppyRequest request13 = new PuppyRequest.PuppyRequestBuilder(Rating.RARE, "바닥에 흘린 강아지", "dog13.jpg", 1.5D, 10D).build();
            result.add(request13);
            PuppyRequest request14 = new PuppyRequest.PuppyRequestBuilder(Rating.UNIQUE, "케르베로스", "dog14.jpg", 1D, 8D).build();
            result.add(request14);
            PuppyRequest request15 = new PuppyRequest.PuppyRequestBuilder(Rating.UNIQUE, "자니…? 강아지", "dog15.jpg", 1D, 8D).build();
            result.add(request15);
            PuppyRequest request16 = new PuppyRequest.PuppyRequestBuilder(Rating.UNIQUE, "토깽강아지", "dog16.jpg", 1D, 8D).build();
            result.add(request16);
            PuppyRequest request17 = new PuppyRequest.PuppyRequestBuilder(Rating.UNIQUE, "영국 유학생 강아지", "dog17.jpg", 1D, 8D).build();
            result.add(request17);
            PuppyRequest request18 = new PuppyRequest.PuppyRequestBuilder(Rating.EPIC, "산신령 강아지", "dog18.jpg", 0.25D, 6.5D).build();
            result.add(request18);
            PuppyRequest request19 = new PuppyRequest.PuppyRequestBuilder(Rating.EPIC, "물개도 강아지야", "dog19.jpg", 0.24D, 6.5D).build();
            result.add(request19);
            PuppyRequest request20 = new PuppyRequest.PuppyRequestBuilder(Rating.LEGENDARY, "파도 강아지", "dog20.jpg", 0.01D, 5D).build();
            result.add(request20);

            result.forEach(item -> {
                Puppy addData = new Puppy.PuppyBuilder(item).build();
                puppyRepository.save(addData);
            });
        }
    }

    public void setFirstHavePuppy() {
        List<HavePuppy> havePuppies = havePuppyRepository.findAll();

        if (havePuppies.size() == 0) {
            List<Puppy> puppies = puppyRepository.findAll();

            puppies.forEach(item -> {
                HavePuppy addData = new HavePuppy.HavePuppyBuilder(item).build();
                havePuppyRepository.save(addData);
            });
        }
    }
}
