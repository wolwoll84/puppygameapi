package com.ksy.gameapi.service;

import com.ksy.gameapi.entity.Ingredient;
import com.ksy.gameapi.exception.CMissingDataException;
import com.ksy.gameapi.model.IngredientResponse;
import com.ksy.gameapi.model.SingleResult;
import com.ksy.gameapi.repository.IngredientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class IngredientService {
    private final IngredientRepository ingredientRepository;

    public IngredientResponse putIngredientPlus() {
        Ingredient ingredient = ingredientRepository.findById(1L).orElseThrow(CMissingDataException::new);
        ingredient.plusIngredient();

        Ingredient result = ingredientRepository.save(ingredient);

        return new IngredientResponse.IngredientResponseBuilder(result).build();
    }
}
