//package com.ksy.gameapi.service;
//
//import com.ksy.gameapi.entity.HavePuppy;
//import com.ksy.gameapi.entity.Ingredient;
//import com.ksy.gameapi.entity.Puppy;
//import com.ksy.gameapi.exception.CMissingDataException;
//import com.ksy.gameapi.exception.CNotEnoughIngredientException;
//import com.ksy.gameapi.model.*;
//import com.ksy.gameapi.repository.HavePuppyRepository;
//import com.ksy.gameapi.repository.IngredientRepository;
//import com.ksy.gameapi.repository.PuppyRepository;
//import lombok.RequiredArgsConstructor;
//import org.springframework.stereotype.Service;
//
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Optional;
//
//@Service
//@RequiredArgsConstructor
//public class ChoosePuppyTwoService { // 안 보고 흐름에 맞춰 코드 작성하기 연습
//    private final IngredientRepository ingredientRepository;
//    private final PuppyRepository puppyRepository;
//    private final HavePuppyRepository havePuppyRepository;
//
//    long chooseIngredient = 0L;
//
//    private void init(boolean isBig) {
//        this.chooseIngredient = isBig ? 35000L : 5000L;
//    } // 뽑기 금액 기준 결정 끝
//
//    public ChoosePuppyResponse getResult(boolean isBig) { // 팀장
//        this.init(isBig); // 가장 먼저 기준 금액 정하고
//
//        boolean isStart = this.isStartByIngredientCheck(); // 돈 있는지 체크하고
//
//        if (!isStart) throw new CNotEnoughIngredientException(); // 돈 없으면 에러 메시지
//
//        Ingredient ingredient = ingredientRepository.findById(1L).orElseThrow(CMissingDataException::new); // 돈 얼마 있는지 확인하려면 데이터 불러와야 하고
//        ingredient.minusIngredient(chooseIngredient); // 돈 차감시키고
//        ingredientRepository.save(ingredient); // 저장하고
//
//        long puppyResultId = this.choosePuppy(isBig); // 유물 뽑고
//
//        Optional<HavePuppy> havePuppy = havePuppyRepository.findByPuppy_Id(puppyResultId); // Optional 로 불러올 것!!
//        if (havePuppy.isEmpty()) throw new CMissingDataException(); // 혹시나 havePuppy 데이터가 비어있을수도 있으니 예외 처리해주고
//        HavePuppy havePuppyData = havePuppy.get(); // .get .get 하기 귀찮으니까 새 부대에 옮겨담기 Optional로 감싸서 그런가봐
//
//        boolean isNewPuppy = false; // 새로운  강아지인지 체크. 기본은 false
//        if (havePuppyData.getPuppyCount() == 0) isNewPuppy = true; // 기존 등록되어 있던 강아지 숫자가 0이면 이번에 처음 뽑은 게 맞으니까 true 로 변경
//        havePuppyData.putCountPlus(); // 뽑았으니 저장해주고
//        havePuppyRepository.save(havePuppyData); // 레포지토리에도 저장해주고
//
//        ChoosePuppyResponse result = new ChoosePuppyResponse();
//        result.setIngredientResponse(new IngredientResponse.IngredientResponseBuilder(ingredient).build());
//        result.setCurrentItem(new ChoosePuppyCurrentItem.ChoosePuppyCurrentItemBuilder(havePuppyData.getPuppy(), isNewPuppy).build());
//        result.setHavePuppyItems(this.getMyPuppies());
//
//        return result;
//    }
//
//    public List<HavePuppyItem> getMyPuppies() { // 내가 보유한 강아지들을 불러오려면... 얜 왜 public 이지?
//        List<HavePuppy> havePuppies = havePuppyRepository.findAll();
//
//        List<HavePuppyItem> result = new LinkedList<>();
//
//        havePuppies.forEach(item -> result.add(new HavePuppyItem.HavePuppyItemBuilder(item).build()));
//
//        return result;
//    }
//
//    private boolean isStartByIngredientCheck() { // 금액 체크하는 애
//        Ingredient ingredient = ingredientRepository.findById(1L).orElseThrow(CMissingDataException::new);
//
//        boolean isStart = false; // 기본 막아두고
//
//        if (ingredient.getIngredientCount() >= chooseIngredient) isStart = true; // 금액 충분하면 통과
//
//        return isStart;
//    }
//
//    private long choosePuppy(boolean isBig) { // 유물 뽑는 애
//        List<Puppy> puppies = puppyRepository.findAll();
//
//        double oldPercent = 0D;
//        List<ChoosePuppyItem> percentBar = new LinkedList<>();
//        for (Puppy puppy : puppies) {
//            ChoosePuppyItem addItem = new ChoosePuppyItem();
//            addItem.setId(puppy.getId());
//            addItem.setPercentMin(oldPercent);
//            if (isBig) {
//                addItem.setPercentMax(oldPercent + puppy.getBigPercent());
//            } else {
//                addItem.setPercentMax(oldPercent + puppy.getCrumbsPercent());
//            }
//
//            percentBar.add(addItem);
//
//            if (isBig) {
//                oldPercent += puppy.getBigPercent();
//            } else {
//                oldPercent += puppy.getCrumbsPercent();
//            }
//        } // 퍼센트바 = 확률표를 만들었어요. 이제 랜덤 돌려서 유물 하나를 뽑게 해야 해요.
//
//        double percentResult = Math.random() * 100; // 0~100 사이의 숫자 하나를 뽑고
//
//        long resultId = 0; // 리턴값 기본 0으로 두고
//        for (ChoosePuppyItem item : percentBar) {
//            if (percentResult >= item.getPercentMin() && percentResult <= item.getPercentMax()) {
//                resultId = item.getId(); // 퍼센트바에 있는 수치와 일치하면 결과값에 넣고
//                break; // 정답 나왔으니 for문 종료
//            }
//        }
//
//        return resultId;
//    }
//}
