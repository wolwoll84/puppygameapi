package com.ksy.gameapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class FirstConnectDataResponse {
    @ApiModelProperty(notes = "재료 정보")
    private IngredientResponse ingredientResponse;

    @ApiModelProperty(notes = "내가 보유한 강아지 리스트")
    private List<HavePuppyItem> havePuppyItems;
}
