package com.ksy.gameapi.model;

import com.ksy.gameapi.entity.Puppy;
import com.ksy.gameapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ChoosePuppyCurrentItem {
    @ApiModelProperty(notes = "등급")
    private String rating;

    @ApiModelProperty(notes = "등급(한글명)")
    private String ratingName;

    @ApiModelProperty(notes = "강아지 이름")
    private String puppyName;

    @ApiModelProperty(notes = "이미지 이름")
    private String imageName;

    @ApiModelProperty(notes = "새로운 강아지 여부")
    private Boolean isNew;

    private ChoosePuppyCurrentItem(ChoosePuppyCurrentItemBuilder builder) {
        this.rating = builder.rating;
        this.ratingName = builder.ratingName;
        this.puppyName = builder.puppyName;
        this.imageName = builder.imageName;
        this.isNew = builder.isNew;
    }

    public static class ChoosePuppyCurrentItemBuilder implements CommonModelBuilder<ChoosePuppyCurrentItem> {
        private final String rating;
        private final String ratingName;
        private final String puppyName;
        private final String imageName;
        private final Boolean isNew;

        public ChoosePuppyCurrentItemBuilder(Puppy puppy, Boolean isNew) {
            this.rating = puppy.getRating().toString();
            this.ratingName = puppy.getRating().getName();
            this.puppyName = puppy.getPuppyName();
            this.imageName = puppy.getImageName();
            this.isNew = isNew;
        }

        @Override
        public ChoosePuppyCurrentItem build() {
            return new ChoosePuppyCurrentItem(this);
        }
    }
}
