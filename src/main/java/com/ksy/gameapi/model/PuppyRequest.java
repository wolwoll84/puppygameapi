package com.ksy.gameapi.model;

import com.ksy.gameapi.enums.Rating;
import com.ksy.gameapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PuppyRequest {
    @ApiModelProperty(notes = "등급", required = true)
    @Enumerated(value = EnumType.STRING)
    @NotNull
    private Rating rating;

    @ApiModelProperty(notes = "강아지 이름 (1~25)", required = true)
    @Length(min = 1, max = 25)
    @NotNull
    private String puppyName;

    @ApiModelProperty(notes = "강아지 이름 (1~50)", required = true)
    @Length(min = 1, max = 50)
    @NotNull
    private String imageName;

    @ApiModelProperty(notes = "부스러기 확률", required = true)
    @NotNull
    private Double crumbsPercent;

    @ApiModelProperty(notes = "푸짐한상 확률", required = true)
    @NotNull
    private Double bigPercent;

    private PuppyRequest(PuppyRequestBuilder builder) {
        this.rating = builder.rating;
        this.puppyName = builder.puppyName;
        this.imageName = builder.imageName;
        this.crumbsPercent = builder.crumbsPercent;
        this.bigPercent = builder.bigPercent;
    }

    public static class PuppyRequestBuilder implements CommonModelBuilder<PuppyRequest> {
        private final Rating rating;
        private final String puppyName;
        private final String imageName;
        private final Double crumbsPercent;
        private final Double bigPercent;

        public PuppyRequestBuilder(Rating rating, String puppyName, String imageName, Double crumbsPercent, Double bigPercent) {
            this.rating = rating;
            this.puppyName = puppyName;
            this.imageName = imageName;
            this.crumbsPercent = crumbsPercent;
            this.bigPercent = bigPercent;
        }

        @Override
        public PuppyRequest build() {
            return new PuppyRequest(this);
        }
    }
}
