package com.ksy.gameapi.model;

import com.ksy.gameapi.entity.HavePuppy;
import com.ksy.gameapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HavePuppyItem {
    @ApiModelProperty(notes = "이미지 이름")
    private String imageName;

    @ApiModelProperty(notes = "강아지 이름")
    private String puppyName;

    @ApiModelProperty(notes = "등급")
    private String rating;

    @ApiModelProperty(notes = "등급(한글명)")
    private String ratingName;

    @ApiModelProperty(notes = "보유한 강아지 수량")
    private Integer puppyCount;

    private HavePuppyItem(HavePuppyItemBuilder builder) {
        this.imageName = builder.imageName;
        this.puppyName = builder.puppyName;
        this.rating = builder.rating;
        this.ratingName = builder.ratingName;
        this.puppyCount = builder.puppyCount;
    }

    public static class HavePuppyItemBuilder implements CommonModelBuilder<HavePuppyItem> {
        private final String imageName;
        private final String puppyName;
        private final String rating;
        private final String ratingName;
        private final Integer puppyCount;

        public HavePuppyItemBuilder(HavePuppy havePuppy) {
            this.imageName = havePuppy.getPuppy().getImageName();
            this.puppyName = havePuppy.getPuppy().getPuppyName();
            this.rating = havePuppy.getPuppy().getRating().toString();
            this.ratingName = havePuppy.getPuppy().getRating().getName();
            this.puppyCount = havePuppy.getPuppyCount();
        }

        @Override
        public HavePuppyItem build() {
            return new HavePuppyItem(this);
        }
    }
}
