package com.ksy.gameapi.model;

import com.ksy.gameapi.entity.Ingredient;
import com.ksy.gameapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class IngredientResponse {
    @ApiModelProperty(notes = "재료 수량")
    private Long ingredientCount;

    private IngredientResponse(IngredientResponseBuilder builder) {
        this.ingredientCount = builder.ingredientCount;
    }

    public static class IngredientResponseBuilder implements CommonModelBuilder<IngredientResponse> {
        private final Long ingredientCount;

        public IngredientResponseBuilder(Ingredient ingredient) {
            this.ingredientCount = ingredient.getIngredientCount();
        }

        @Override
        public IngredientResponse build() {
            return new IngredientResponse(this);
        }
    }
}
