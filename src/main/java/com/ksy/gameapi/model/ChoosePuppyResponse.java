package com.ksy.gameapi.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ChoosePuppyResponse {
    @ApiModelProperty(notes = "재료 정보")
    private IngredientResponse ingredientResponse;

    @ApiModelProperty(notes = "뽑은 강아지 정보")
    private ChoosePuppyCurrentItem currentItem; // 얘는 왜 response 가 아니라 item 이지? ... 연속뽑기? 마즘!!

    @ApiModelProperty(notes = "내가 보유한 강아지 리스트")
    private List<HavePuppyItem> havePuppyItems;
}
