package com.ksy.gameapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Rating {
    NORMAL("노말")
    , RARE("레어")
    , UNIQUE("유니크")
    , EPIC("에픽")
    , LEGENDARY("레전더리")
    ;

    private final String name;
}
