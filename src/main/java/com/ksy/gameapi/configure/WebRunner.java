package com.ksy.gameapi.configure;

import com.ksy.gameapi.service.InitDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component // @Component는 개발자가 직접 작성한 class를 Bean으로 등록해주는 애고,
// Application Runner는 특정 Bean을 Application 실행 후 실행하도록 하는 인터페이스구나. class에서 interface를 사용하니까 implements가 붙는구나.
@RequiredArgsConstructor // 얜 서비스랑 연결해야 하니까 붙어야 하는 거 알겠어
public class WebRunner implements ApplicationRunner { // 얘가 implements여야 하는 이유 : class에서 interface를 사용할 땐 implements를 쓴다
    private final InitDataService initDataService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        initDataService.setFirstIngredient();
        initDataService.setFirstPuppy();
        initDataService.setFirstHavePuppy();
    }
}
