package com.ksy.gameapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
