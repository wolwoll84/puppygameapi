package com.ksy.gameapi.repository;

import com.ksy.gameapi.entity.Puppy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PuppyRepository extends JpaRepository<Puppy, Long> {
}
