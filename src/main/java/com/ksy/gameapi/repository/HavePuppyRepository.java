package com.ksy.gameapi.repository;

import com.ksy.gameapi.entity.HavePuppy;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface HavePuppyRepository extends JpaRepository<HavePuppy, Long> {
    Optional<HavePuppy> findByPuppy_Id(long puppyId);
    List<HavePuppy> findAllByIdGreaterThanEqualOrderByIdDesc(long id);
}
